***************************
***************************

# **디렉토리 관리 공통 모듈**

# 목차 
###  1. 개요 
  * 1.1 지원대상 
  * 1.2 기능흐름도 
###  2. 설명  
  * 2.1 클래스다이어그램 
  * 2.2 관려소스 
###  3. 관련 기능 
  * 3.1 관리자 로그인 
  * 3.2 디렉토리 관리(생성,삭제,수정) 
  * 3.3 파일 관리(업로드,다운로드,로고이미지 관리) 
  * 3.4 로그 기능 
### 4. 참고자료


***************************
***************************
#### 1.개요 ####
* 디렉토리 관리 서비스는 사용자가 실제 사용하는 컴퓨터 내 폴더를 다른 사용자와 공유할 수 있게끔 해주는 웹 기반의 서비스이다.
디렉토리 관리 서비스는 Spring Framework 4.0 기반으로 만들어졌다.
JSP File Browser, jquery tree plugin(jstree), jquery contextmenu를
기반으로 만들어진 웹 공통 컴포넌트이다.
#### 1.1 지원대상 ####

* 본 컴포넌트는 웹 브라우저를 대상으로 개발 및 테스트를 완료했다.
Internet Explorer 버전 11이상과 Chrome 브라우저에서 작동이 잘 되는 것을
확인하였다. 파일 이미지는 실제 컨테이너 환경이 아닌 C드라이브나 D드라이브 내에서
불러오게 되있어서 보안상의 문제로 이미지 불러들일 때 오류가 생길 수도 있다.
확인 시에는 Spring Tool Suite Internal Browser내에서 확인하거나 img src
경로명을 바꾸는 것을 추천한다.

#### 1.2 기능흐름도 ####
![시퀀스다이어그램1.png](https://bitbucket.org/repo/LKpbXa/images/3631007204-%EC%8B%9C%ED%80%80%EC%8A%A4%EB%8B%A4%EC%9D%B4%EC%96%B4%EA%B7%B8%EB%9E%A81.png)

[일반 사용자 흐름도]

![시퀀스다이어그램2.png](https://bitbucket.org/repo/LKpbXa/images/2437827839-%EC%8B%9C%ED%80%80%EC%8A%A4%EB%8B%A4%EC%9D%B4%EC%96%B4%EA%B7%B8%EB%9E%A82.png)

[관리자 흐름도]
***************************
***************************
#### 2. 설명 ####

* 디렉토리 관리 기능은 크게 관리자 로그인 기능, 디렉토리 관리 기능(생성,수정,삭제)
파일 관리 기능이(업로드,다운로드,삭제,이미지 수정) 있다.
#### 2.1 클래스 다이어그램 ####

![클래스다이어그램.png](https://bitbucket.org/repo/LKpbXa/images/2693764692-%ED%81%B4%EB%9E%98%EC%8A%A4%EB%8B%A4%EC%9D%B4%EC%96%B4%EA%B7%B8%EB%9E%A8.png)

#### 2.2 관련 소스 ####

![1.PNG](https://bitbucket.org/repo/LKpbXa/images/1364792343-1.PNG)

***************************
***************************

#### 3 관련 기능 ####

#### 3.1 관리자 로그인 ####

#### 3.1.1 비즈니스 규칙 ####

* 관리자 로그인시에 데이터베이스를 사용하는게 아니라 C드라이브 밑에 etc.properties를
사용하여 관리자 아이디 및 암호를 저장한다.
properties안에 아이디 및 암호 저장시 SHA-256을 사용해서 암호화 변환 후 저장한다.

#### 3.1.2 관련하면 및 수행 메뉴얼 ####

![1.PNG](https://bitbucket.org/repo/LKpbXa/images/650229804-1.PNG)

![1.PNG](https://bitbucket.org/repo/LKpbXa/images/645242752-1.PNG)

***************************


#### 3.2 디렉토리 관리 ####

#### 3.2.1 비즈니스 규칙 ####

* 디렉토리 관리는 관리자로 로그인을 해야 관련 기능을 사용 할 수 있다. 오른쪽 Checkbox
를 클릭하여 해당 디렉토리를 Delete,Rename할 수 있다.

또한 input에 디렉토리 name을 넣고 Create시에 디렉토리를 생성 할 수 있고
디렉토리 화면 내에서 오른쪽 클릭을 하여 해당 디렉토리를 삭제 할 수 있다.
주의 할 점이 한가지 있는데, 디렉토리 생성이나 오른쪽 버튼을 클릭하여 삭제 할 시에
현재 작업 디렉토리 기준은 쿠키에 저장된 현재 위치를 확인하고 작업해야 한다.

#### 3.2.2 관련화면 및 수행 메뉴얼 ####

![1.PNG](https://bitbucket.org/repo/LKpbXa/images/279147497-1.PNG)

![2.PNG](https://bitbucket.org/repo/LKpbXa/images/3997340804-2.PNG)

***************************

#### 3.3 파일 관리 ####

#### 3.3.1 비즈니스 규칙 ####

* 파일 관리는 크게 4가지로 다운로드,업로드,이미지,properties 기능으로 구분된다.
특정 파일을 업로드 하는 것은 관리자로 로그인시에만 가능하고 다운로드는 관리자 로그인
없이도 가능하다.
이미지는 해당 디렉토리 내 이미지를 정할 수 있으며, 업로드 시에 파일명이 logo.jpg로
변경이 되서 업로드 하게 된다.
properties기능은 해당 디렉토리가 생성될시에 기본적으로 만들어지고 해당 디렉토리의
상세 설명을 할 수 있는 기능들을 포함한다. properties를 생성, 수정, 삭제 할 수 있는
기능 또한 있다

#### 3.3.2 관련화면 및 수행 메뉴얼 ####


![1.PNG](https://bitbucket.org/repo/LKpbXa/images/3167512272-1.PNG)

![2.PNG](https://bitbucket.org/repo/LKpbXa/images/531710502-2.PNG)


***************************

#### 3.4 로그 ####

#### 3.4.1 비즈니스 규칙 ####

* 로그 기능은 Controller에서 특정 메소드 수행시에 log를 남긴다
크게 관리자 로그인시에, 파일 및 디렉토리 관련 기능 사용시에 해당 사용기능과
아이피를 남기게 된다.
로그 파일은 D드라이브 안에 log폴더를 생성한후 log.log파일이 자동으로 생성되게 한다.
Console과 file에 log를 찍게 되고 10Mb 크기 이상으로 넘어가게 되면
자동으로 백업하게 된다.

#### 3.4.2 관련 화면 ####

![1.PNG](https://bitbucket.org/repo/LKpbXa/images/21082065-1.PNG)

![2.PNG](https://bitbucket.org/repo/LKpbXa/images/3636971726-2.PNG)


***************************
***************************

#### 4. 참고 자료 ####

* [File Browser](http://www.vonloesch.de/filebrowser.html)
* [Js Tree](http://old.jstree.com)
* [Jquery Context menu](http://plugins.jquery.com/tag/context-menu)
* [전자정부프레임워크](http://www.egovframe.go.kr)
* [spring framework](http://spring.io/guides)