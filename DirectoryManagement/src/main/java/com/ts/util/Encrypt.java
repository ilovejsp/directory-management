package com.ts.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ts.controller.MainController;


public class Encrypt {
	private static final Logger logger = LoggerFactory.getLogger(Encrypt.class);
	
	//SHA-256을 이용한 암호화 
	public static String getEncrypt(String input){
		String SHA = "";
		try {
			MessageDigest sh = MessageDigest.getInstance("SHA-256");
			sh.update(input.getBytes());
			byte byteData[] = sh.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
						.substring(1));
			}
			SHA = sb.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			SHA = null;
		}
		logger.info("getEncrypt method Call SHA VALUE : "+SHA);
		return SHA;
	}
	
}
