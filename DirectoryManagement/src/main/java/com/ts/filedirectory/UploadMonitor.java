package com.ts.filedirectory;
import com.sun.org.apache.xalan.internal.xsltc.runtime.Hashtable;

public class UploadMonitor {
	public static Hashtable uploadTable = new Hashtable();
	public static void set(String fName, UpInfo info) {
		uploadTable.put(fName, info);
	}
	public static void remove(String fName) {
		uploadTable.remove(fName);
	}
	public static UpInfo getInfo(String fName) {
		UpInfo info = (UpInfo) uploadTable.get(fName);
		return info;
	}
}
