package com.ts.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspFactory;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ts.filedirectory.FileDirectoryManager;
import com.ts.filedirectory.Writer2Stream;
import com.ts.util.DownloadUtil;
import com.ts.util.Utils;

@Controller
public class FileController  {
	private static final Logger logger = LoggerFactory
			.getLogger(FileController.class);
	// stub code
	private FileDirectoryManager fdManager;

	@RequestMapping("/fileCreate")
	public void fileCreate(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		logger.info("/fileCreate page Call");

		String dir = request.getParameter("dir");
		logger.info("request directory path from IP : "
				+ request.getRemoteAddr());

		FileWriter fw = new FileWriter(dir + "\\basic.properties");
		fw.write("#this is basic file included name,explain,url,version,registerdate,modifydate\n");
		fw.write("name=write your solution's name\n");
		fw.write("explain=tell me about your solutions\n");
		fw.write("url=This is URL link\n");
		fw.write("version=what's your version level?\n");
		fw.write("registerdate=2015-01-11\n");
		fw.write("modifydate=2015-01-11\n");
		fw.close();
		response.sendRedirect("index");
	}

	@RequestMapping("/fileUpdate")
	public ModelAndView fileUpdate(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		logger.info("/fileUpdate page Call");
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/main/fileUpdate");
		return mv;
	}

	@RequestMapping("/fileDelete")
	public void fileDelete(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		logger.info("/fileDelete page Call");
		logger.info("request filedelete from IP : " + request.getRemoteAddr());

		File file = new File(request.getParameter("dir") + "\\basic.properties");
		file.delete();
		response.sendRedirect("index");
	}

	@RequestMapping("/fileUpdateComplete")
	public void fileUpdateComplete(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		logger.info("/fileUpdateComplete page Call");
		logger.info("request fileupdate from IP : " + request.getRemoteAddr());
		logger.info("fileUpdateComplete : "+request.getParameter("dir"));
		String name = request.getParameter("name");
		System.out.println("name check  :"+name);
		String explain = request.getParameter("explain");
		String url = request.getParameter("url");
		String version = request.getParameter("version");
		String rdate = request.getParameter("rdate");
		String mdate = request.getParameter("mdate");
		String dir = request.getParameter("dir");

		File nFile = new File(request.getParameter("dir")+"\\basic.properties");
		FileWriter fw = new FileWriter(dir + "\\basic.properties");

		fw.write("#this is basic file included name,explain,url,version,registerdate,modifydate\n");
		fw.write("name=" + name + "\n");
		fw.write("explain=" + explain + "\n");
		fw.write("url=" + url + "\n");
		fw.write("version=" + version + "\n");
		fw.write("registerdate=" + rdate + "\n");
		fw.write("modifydate=" + mdate + "\n");
		fw.close();

		response.sendRedirect("index");
	}

	@RequestMapping("/download")
	public void download(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		logger.info("/download page Call");
		logger.info("request download from IP : " + request.getRemoteAddr());
        File f = new File(request.getParameter("downfile"));
		DownloadUtil.download(request, response, f);
		response.sendRedirect("index");
			
	}



}