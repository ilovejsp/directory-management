package com.ts.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import com.ts.filedirectory.FileDirectoryManager;
import com.ts.filedirectory.FileInfo;
import com.ts.filedirectory.HttpMultiPartParser;
import com.ts.filedirectory.UpInfo;
import com.ts.filedirectory.UploadMonitor;
import com.ts.filedirectory.Writer2Stream;
import com.ts.util.Utils;

@Controller
public class DirectoryController {
	private static final Logger logger = LoggerFactory.getLogger(DirectoryController.class);

	// stub code
	private FileDirectoryManager fdManager;

	@RequestMapping("/directoryCreate")
	public void directory(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession(true);
		logger.info("/directoryCreate page Call");
		logger.info("request directoryCreate from IP : " + request.getRemoteAddr());

		if (request.getParameter("Submit") != null && request.getParameter("Submit").equals(Utils.CREATE_DIR)) {
			String dir =(String)session.getAttribute("dir");
			System.out.println("dir attr: "+request.getAttribute("dir"));
			
			System.out.println("dir : "+request.getParameter("dir"));
			String dir_name = request.getParameter("cr_dir");
			System.out.println("cr_dir : "+request.getParameter("cr_dir"));
			String new_dir = Utils.getDir(dir, dir_name);
			if (!Utils.isAllowed(new File(new_dir))) {
				logger.error("You are not allowed to access " + new_dir);
			} else {
				new File(new_dir).mkdirs();
				FileWriter fw = new FileWriter(new_dir + "\\basic.properties");
				fw.write("#this is basic file included name,explain,url,version,registerdate,modifydate\n");
				fw.write("name=write your solution's name\n");
				fw.write("explain=tell me about your solutions\n");
				fw.write("url=This is URL link\n");
				fw.write("version=what's your version level?\n");
				fw.write("registerdate=When is your register date?\n");
				fw.write("modifydate=When is your latest modified date?\n");
				fw.close();
			}
		}
		response.sendRedirect("index");
	}

	@RequestMapping("/directoryDelete")
	public void directoryDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession(true);

		logger.info("/directoryDelete page Call");
		logger.info("request directoryDelete from IP : " + request.getRemoteAddr());

		Vector v = Utils.expandFileList(request.getParameterValues("selfile"), true);
		System.out.println("check : " + request.getParameterValues("selfile"));

		boolean error = false;
		for (int i = v.size() - 1; i >= 0; i--) {
			File f = (File) v.get(i);
			if (!Utils.isAllowed(f)) {
				logger.error("You are not allowed to access ");
				error = true;
				break;
			}
			if (!f.canWrite() || !f.delete()) {
				logger.error("Deletion aborted");
				error = true;
				break;
			}
		}
		if ((!error) && (v.size() > 1)) {
			logger.error("All files deleted");
		} else if ((!error) && (v.size() > 0)) {
			logger.error("File deleted");
		} else if (!error) {
			logger.error("No files selected");
		}
		session.setAttribute("dir", Utils.DEFAULT_ROOT);
		response.sendRedirect("index");
	}
	@RequestMapping("/directoryDeleteFromRightClick")
	public void directoryDeleteFromRightClick(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.info("/directoryDeleteFromRightClick page Call");
		logger.info("request directoryDeleteFromRightClick from IP : "+request.getRemoteAddr());
		HttpSession session = request.getSession(true);
		File file = new File((String)session.getAttribute("dir"));
		logger.info("directoryDeleteFromRightClick from session "+session.getAttribute("dir"));
		Utils.deleteDirectory((String)session.getAttribute("dir"));
		session.setAttribute("dir", Utils.DEFAULT_ROOT);
		
		Cookie[] cookies = request.getCookies();  
		response.sendRedirect("index");	
	}

	@RequestMapping("/directoryRename")
	public void directoryRename(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session=request.getSession(true);
		logger.info("/directoryRename page Call");
		logger.info("request directoryRename from IP : "+request.getRemoteAddr());
		logger.info("directoryRename dir :"+session.getAttribute("dir"));
		logger.info("directoryRename cr_dir :"+request.getParameter("cr_dir"));
		
		Vector v = Utils.expandFileList(request.getParameterValues("selfile"), true);
		String dir=(String)session.getAttribute("dir");
		String new_file_name = request.getParameter("cr_dir");
		String new_file = Utils.getDir(dir, new_file_name);
		logger.info("directoryRename new_file :"+new_file);
		

		File f = (File) v.get(0);
		if (!Utils.isAllowed(f)) {
			logger.error("You are not allowed to access");
		}
		else if ((new_file.trim() != "") && !new_file.endsWith(File.separator)) {
			if (!f.canWrite() || !f.renameTo(new File(new_file.trim()))) {
				logger.error("Creation of file ");
			} else {
				logger.info("Renamed file " + ((File) v.get(0)).getName() + " to " + new_file);
			}
		} else {
			logger.error("Error: \"" + new_file_name + "\" is not avalid filename");
		}
		response.sendRedirect("index");
	}

	@RequestMapping("/upload")
	public void upload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.info("/upload page Call");
		logger.info("request upload from IP : "+request.getRemoteAddr());
		response.setContentType("text/html");
		HttpMultiPartParser parser = new HttpMultiPartParser();
		boolean error = false;
		try {
			int bstart = request.getContentType().lastIndexOf("oundary=");
			String bound = request.getContentType().substring(bstart + 8);
			int clength = request.getContentLength();
			Hashtable ht = parser.processData(request.getInputStream(), bound, Utils.tempdir, clength);
			if (!Utils.isAllowed(new File((String) ht.get("dir")))) {
				logger.error("You are not allowed to access " + ht.get("dir"));
				error = true;
			} else if (ht.get("myFile") != null) {
				FileInfo fi = (FileInfo) ht.get("myFile");
				File f = fi.file;
				UpInfo info = UploadMonitor.getInfo(fi.clientFileName);
				if (info != null && info.aborted) {
					f.delete();
					logger.error("Upload aborted");
				} else {
					String path = (String) ht.get("dir");
					if (!path.endsWith(File.separator))
						path = path + File.separator;
					if (!f.renameTo(new File(path + f.getName()))) {
						logger.error("Cannot upload file.");
						error = true;
						f.delete();
					}
				}
			} else {
				logger.error("No file selected for upload");
				error = true;
			}
			//request.setAttribute("dir", (String) ht.get("dir"));
		} catch (Exception e) {
			logger.error("Error " + e + ". Upload aborted");
			error = true;
		}
		if (!error)
			logger.info("File upload correctly finished.");

		response.sendRedirect("index");
	}
	@RequestMapping("/logoUpdate")
	public void logoUpdate(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.info("/logoUpdate page Call");
		logger.info("request logoUpdate from IP : "+request.getRemoteAddr());
		response.setContentType("text/html");
	    request.setCharacterEncoding("UTF-8");
	    
	    String uploadFile = "";
	    int read = 0;
	    byte[] buf = new byte[1024];
	    FileInputStream fin = null;
	    FileOutputStream fout = null;
	    MultipartRequest multi = new MultipartRequest(request, request.getParameter("dir"), 1024*1024*10, "UTF-8", new DefaultFileRenamePolicy());
	    try{
	        uploadFile = multi.getFilesystemName("uploadFile");
	        File oldFile = new File(request.getParameter("dir") + uploadFile);
	    }catch(Exception e){
	        e.printStackTrace();
	    }
		response.sendRedirect("index");
	}

	@RequestMapping("/browser")
	public ModelAndView browser() {
		logger.info("/browser page Call");
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/directory/browser");
		return mv;
	}

}