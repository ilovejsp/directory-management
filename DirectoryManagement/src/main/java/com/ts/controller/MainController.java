package com.ts.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.experimental.theories.Theories;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ts.util.Encrypt;
import com.ts.util.Utils;

@Controller
public class MainController {
	
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);
	
	

	@RequestMapping("/index")
	public ModelAndView home() {
		logger.info("/index page Call");
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/main/index");
		return mv;
	}

	@RequestMapping("/admin")
	public ModelAndView admin() {
		logger.info("/admin page Call");
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/main/admin");

		return mv;
	}
	@RequestMapping("/admin_login")
	public void admin_login(HttpServletRequest request,HttpServletResponse response) throws IOException {
		logger.info("/admin_login Call, Ip : "+request.getRemoteAddr());
	    String id=request.getParameter("id");  
        String password=request.getParameter("password");  
		logger.info("/admin_login Call, id input : "+id+"pass input : "+password);      
        if(Encrypt.getEncrypt(id).equals(Utils.getProperty("id")) && Encrypt.getEncrypt(password).equals(Utils.getProperty("pass"))){
        	request.getSession().setAttribute("admin", "admin");
    		logger.info("Get session from  Ip : "+request.getRemoteAddr());
        }  
        response.sendRedirect("index");
	}

	@RequestMapping("/detail")
	public ModelAndView detail() {
		logger.info("/detail Call");
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/main/detail");

		return mv;
	}

	@RequestMapping("/detailCreate")
	public ModelAndView detailCreate() {
		logger.info("/detailCreate Call");

		ModelAndView mv = new ModelAndView();
		mv.setViewName("/main/detailCreate");

		return mv;
	}

	@RequestMapping("/detailModify")
	public ModelAndView detailModify() {
		logger.info("/detailModify Call");

		ModelAndView mv = new ModelAndView();
		mv.setViewName("/main/detailModify");
		return mv;
	}

	@RequestMapping("/detailDelete")
	public ModelAndView detailDelete() {
		logger.info("/detailDelete Call");

		ModelAndView mv = new ModelAndView();
		mv.setViewName("/main/detailDelete");
		return mv;
	}

	@RequestMapping("/logout")
	public void logout(HttpServletRequest request,HttpServletResponse response) throws IOException {
		logger.info("/logout Call from ip : "+request.getRemoteAddr());
		request.getSession().invalidate();
		response.sendRedirect("index");
	}

}
