package com.oreilly.servlet.multipart;

import java.io.File;
import java.io.IOException;

/**
 * 
 * @author ilovejsp 
 * 이미지 업로드할때마다 동일한 이름으로 upload하게 만드는 기능
 */
public class DefaultFileRenamePolicy implements FileRenamePolicy {
	public File rename(File f) {
		if (createNewFile(f)) {
			return f;
		}
		String name = "logo.jpg";

		f = new File(f.getParent(), name);

		return f;
	}

	private boolean createNewFile(File f) {
		try {
			f = new File(f.getParent(), "logo.jpg");
			return f.createNewFile();
		} catch (IOException ignored) {
		}
		return false;
	}
}