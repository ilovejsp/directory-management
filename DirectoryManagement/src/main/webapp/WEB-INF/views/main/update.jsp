<%@page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@page import="com.ts.util.Utils"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
</head>
<%
	request.setCharacterEncoding("UTF-8");
	String dir = (String)session.getAttribute("dir");
	String fdir = dir + "basic.properties";
%>
<body>
	<div>
		<form action="fileUpdateComplete?dir=<%=dir%>" method="post">
			<ul>
				<li><h2 align="center">
						<ins>
							솔루션 이름 : <input type="text" name="name" value="<%=Utils.getProperty(fdir, "name")%>" required>
						</ins>
					</h2></li>
				<li>설명 :</li>
				<li><textarea rows="5" cols="50" name="explain"  required><%=Utils.getProperty(fdir, "explain")%></textarea>
				</li>
				<li>URL : <input type="url" name="url" value="<%=Utils.getProperty(fdir, "url")%>" required></li>
				<li>버전 : <input type="text" name="version" value="<%=Utils.getProperty(fdir, "version")%>" required></li>
				<li>등록한 날짜 : <input type="date" name="rdate" value="<%=Utils.getProperty(fdir, "registerdate")%>" required></li>
				<li>마지막 수정 날짜 : <input type="date" name="mdate" value="<%=Utils.getProperty(fdir, "modifydate")%>" required></li>
				<li>
				<c:if test="${sessionScope.admin !=null}">
					<input type="submit" value="수정완료">
				</c:if>
				</li>
			</ul>
			
		</form>
	</div>
</body>
</html>