<%@page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@page import="java.io.File"%>
<%@page import="com.ts.util.Utils"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
</head>
<%
if(session.getAttribute("dir")==null || session.getAttribute("dir").equals("")){
	session.setAttribute("dir", Utils.DEFAULT_ROOT);
}
String dir=(String)session.getAttribute("dir");
//System.out.print("dir check : "+dir);
String fdir=dir+"basic.properties";
%>
<body>
쿠키에 저장된 현재 위치 : <%=session.getAttribute("dir") %>
<div>
 <ul>
    <li><h2 align="center"><ins>솔루션 이름 : <%=Utils.getProperty(fdir,"name") %> </ins><img src="<%=dir%>logo.jpg" height="50" width="50">
    		<c:if test="${sessionScope.admin !=null}">
    		<a href="fileCreate?dir=<%=dir%>">생성|</a><a href="fileUpdate?dir=<%=dir%>">수정|</a><a href="fileDelete?dir=<%=dir%>">삭제</a>
				<form name="fileForm" id="fileForm" method="POST" action="/ts/logoUpdate?dir=<%=dir%>" enctype="multipart/form-data">
				    <input type="file" name="uploadFile" id="uploadFile"> 
				    <input type="submit" value="전송">
				</form>				
    		</c:if>
    	</h2>
    </li>
    <li>설명 :<%=Utils.getProperty(fdir,"explain") %></li>
    <li>URL : <a href="<%=Utils.getProperty(fdir,"url") %>"><%=Utils.getProperty(fdir,"url") %></a></li>
    <li>버전 : <%=Utils.getProperty(fdir,"version") %></li>
    <li>등록한 날짜 : <%=Utils.getProperty(fdir,"registerdate") %></li>
    <li>마지막 수정 날짜 : <%=Utils.getProperty(fdir,"modifydate") %></li>
    
  </ul>
</div>

<%

File f = new File(dir);
if(f.exists()){
	File[] entry =f.listFiles();
	for (int i = 0; i < entry.length; i++) {
		if(entry[i].isFile() && !entry[i].getName().equals("basic.properties") && !entry[i].getName().equals("logo.jpg")){
			out.println("다운 : <a href=\"/ts/download?downfile="+dir+entry[i].getName()+"\">"+entry[i].getName()+"</a>"+"<br>");
		}
	}
}
%>

</body>
</html>