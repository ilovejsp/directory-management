<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
<link href="<c:url value="/resources/css/loginstyle.css"/>" rel="stylesheet">
</head> 
<body>
  <section class="container">
    <div class="login">
      <h1>관리자로그인</h1>
      <form method="post" action="admin_login">
        <p><input type="text" name="id" placeholder="관리자 아이디"></p>
        <p><input type="password" name="password" placeholder="비밀번호"></p>
        <p class="submit"><input type="submit" name="commit" value="로그인"></p>
      </form>
    </div>
  </section>
</body>
</html>