<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>top</title>
</head>
<body>
	<div>
		<h1><a href="index">파일 디렉토리 관리 메인페이지</a></h1>
		<p>
		<c:if test="${sessionScope.admin !=null}">관리자님 환영합니다<a href="/ts/logout">로그아웃</a></c:if>
		<c:if test="${sessionScope.admin ==null}"><a href="/ts/admin">관리자 로그인</a></c:if>
		</p>
	</div>

</body>
</html>