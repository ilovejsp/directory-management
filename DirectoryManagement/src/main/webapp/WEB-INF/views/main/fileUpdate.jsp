<%@page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
<link href="/ts/resources/css/main.css" rel="stylesheet">
</head>
<body>
	<div id="wapper">
		<!-- top 시작 -->
		<header>
		<c:import url="top.jsp" /></header>
		<!-- top 끝-->

		<!-- main left 시작 -->
		<aside>
		<div class="dscroll">
			<c:import url="../directory/browser.jsp" />
		</div>
		</aside>
		<!-- main left 끝-->

		<!-- main content 시작 -->
		<section>
		<article>
		<div class="dscroll">
			<c:import url="update.jsp" />
		</div>
		</article> 
		</section>
		<!-- main content 끝 -->
		
		<!-- bottm 시작 -->
		<footer>
		<c:import url="bottom.jsp" /></footer>
		<!-- bottm 끝 -->
	</div>
</body>
</html>
