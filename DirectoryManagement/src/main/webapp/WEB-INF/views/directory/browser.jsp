<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@page import="java.util.*,java.net.*,java.text.*,java.util.zip.*,java.io.*"%>
<%@page import="com.ts.filedirectory.*" %>
<%@page import="com.ts.util.*" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
response.setHeader("Cache-Control","no-store");   
response.setHeader("Pragma","no-cache");   
response.setDateHeader("Expires",0);   
if (request.getProtocol().equals("HTTP/1.1")){
    response.setHeader("Cache-Control", "no-cache"); 
}
if(session.getAttribute("dir")==null || session.getAttribute("dir").equals("")){
	session.setAttribute("dir", Utils.DEFAULT_ROOT);
}

%>

	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>jQuery File Tree Demo</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<style type="text/css">
			BODY,
			HTML {
				padding: 0px;
				margin: 0px;
			}
			BODY {
				font-family: Verdana, Arial, Helvetica, sans-serif;
				font-size: 11px;
				background: #EEE;
				padding: 15px;
			}
			
			H1 {
				font-family: Georgia, serif;
				font-size: 20px;
				font-weight: normal;
			}
			
			H2 {
				font-family: Georgia, serif;
				font-size: 16px;
				font-weight: normal;
				margin: 0px 0px 10px 0px;
			}
			
			.example {
				float: left;
				margin: 15px;
			}
			
			.demo {
				width: 200px;
				height: 400px;
				border-top: solid 1px #BBB;
				border-left: solid 1px #BBB;
				border-bottom: solid 1px #FFF;
				border-right: solid 1px #FFF;
				background: #FFF;
				overflow: scroll;
				padding: 5px;
			}
			
		</style>
		<script src="/ts/resources/filetree/jquery.js" type="text/javascript"></script>
		<script src="/ts/resources/filetree/jquery.easing.js" type="text/javascript"></script>
		<script src="/ts/resources/filetree/jqueryFileTree.js" type="text/javascript"></script>
		<link href="/ts/resources/filetree/jqueryFileTree2.css" rel="stylesheet" type="text/css" media="screen" />
		
		<script src="/ts/resources/jquery.contextMenu-master/jquery-1.7.js" type="text/javascript"></script>
		<script src="/ts/resources/jquery.contextMenu-master/jquery.contextMenu.js" type="text/javascript"></script>
		<link href="/ts/resources/jquery.contextMenu-master/jquery.contextMenu.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript">
			$(document).ready( function() {
				$('#fileTreeDemo_4').fileTree({ root: 'D:\dm', script: '/ts/resources/filetree/connectors/jqueryFileTree.jsp', folderEvent: 'click', expandSpeed: 1, collapseSpeed: 1 }, function(file) { 
				});
				
				$("#fileTreeDemo_4").contextMenu({
					menu: 'myMenu'
				}, function(action, el, pos) {
					alert("삭제할려는 파일 : "+'<%=session.getAttribute("dir")%>');
					document.location.href="/ts/directoryDeleteFromRightClick";
				});
				
				// Enable menus
				$("#enableMenus").click( function() {
					$('#myDiv, #fileTreeDemo_4').enableContextMenu();
					$(this).attr('disabled', true);
					$("#disableMenus").attr('disabled', false);
				});
				
				// Enable cut/copy
				$("#enableItems").click( function() {
					$('#myMenu').enableContextMenuItems('#cut,#copy');
					$(this).attr('disabled', true);
					$("#disableItems").attr('disabled', false);
				});			

			});
								
			var check = false;
			<%// Disables the checkbox feature %>
			function dis(){check = true;}
			var DOM = 0, MS = 0, OP = 0, b = 0;
			<%// Determine the browser type %>
			function CheckBrowser(){
				if (b == 0){
					if (window.opera) OP = 1;
					// Moz or Netscape
					if(document.getElementById) DOM = 1;
					// Micro$oft
					if(document.all && !OP) MS = 1;
					b = 1;
				}
			} 
			<%// Allows the whole row to be selected %>
			function selrow (element, i){
				var erst;
				CheckBrowser();
				if ((OP==1)||(MS==1)) erst = element.firstChild.firstChild;
				else if (DOM==1) erst = element.firstChild.nextSibling.firstChild;
				<%// MouseIn %>
				if (i==0){
					if (erst.checked == true) element.className='mousechecked';
					else element.className='mousein';
				}
				<%// MouseOut %>
				else if (i==1){
					if (erst.checked == true) element.className='checked';
					else element.className='mouseout';
				}
				<%    // MouseClick %>
				else if ((i==2)&&(!check)){
					if (erst.checked==true) element.className='mousein';
					else element.className='mousechecked';
					erst.click();
				}
				else check=false;
			}
			<%//(De)select all checkboxes%>
			function AllFiles(){
				for(var x=0;x<document.FileList.elements.length;x++){
					var y = document.FileList.elements[x];
					var ytr = y.parentNode.parentNode;
					var check = document.FileList.selall.checked;
					if(y.name == 'selfile'){
						if (y.disabled != true){
							y.checked = check;
							if (y.checked == true) ytr.className = 'checked';
							else ytr.className = 'mouseout';
						}
					}
				}
			}
			
			function clicked(rel){
				 //window.location.reload();
				 var rel = rel.getAttribute("rel");
				 var dir='<%=session.getAttribute("dir")%>';
				 document.cookie="dir="+escape(rel);
				 //history.go(0);
				 //window.location.reload();
			}

		</script>
		
	</head>
	
	<body>
	<%
	
	Cookie[] cookies = request.getCookies();  
	                 
	for(int i=0; i<cookies.length; i++){
		if(cookies[i].getName().equals("dir") && !cookies[i].getValue().contains(".") && cookies[i].getValue().contains("root")){
			session.removeAttribute("dir");
			session.setAttribute("dir", Utils.unescape(cookies[i].getValue()));
		}
	}
	
	%>
	
	<form method="Post" name="FileList" action="">
		<div class="example">
			<div id="fileTreeDemo_4" class="demo">	
			</div>
		</div>
	<c:if test="${sessionScope.admin !=null}">	
	<p>
	
		<input type="hidden" name="dir" value="<%=session.getAttribute("dir")%>">
		<input type="hidden" name="sort" value="1">
	</p>
	
	<p>
		<input title="Enter new dir or filename or the relative or absolute path" type="text" name="cr_dir">
		<input onclick='this.form.action="/ts/directoryCreate";' title="Create a new directory with the given name" class="button" type="submit" name="Submit" value="<%=Utils.CREATE_DIR%>">
		<input onclick='this.form.action="/ts/directoryDelete";' title="delete a file,directory" class="button" type="submit" name="Submit" value="<%=Utils.DELETE_FILES%>">
		<input onclick='this.form.action="/ts/directoryRename?dir=<%=session.getAttribute("dir")%>";' title="Rename selected file or directory to the entered name" class="button" type="Submit" name="Submit" value="<%=Utils.RENAME_FILE%>">
		<input type="hidden" name="dir" value="<%=session.getAttribute("dir")%>">
	</p>
	</form>
	<form action="/ts/upload" enctype="multipart/form-data" method="POST">
		<input type="hidden" name="dir" value="<%=session.getAttribute("dir")%>">
		<input type="hidden" name="sort" value="1">
		<input type="file" name="myFile">
		<input title="Upload selected file to the current working directory" type="Submit" class="button" name="Submit" value="Upload">
	</form>
</c:if>

	<hr>
		<ul id="myMenu" class="contextMenu">
			<li class="delete"><a href="#delete">Delete</a></li>
		</ul>
</body>
</html>
   
            
		
